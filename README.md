# Zayssystem
write a express or dns server in 3 line!

## Install here
```
  npm i zayssystem
  
```
or go to [Current Releases](https://github.com/KTBZay/Zayssystem/releases)

# How to use (Express/Dns - 32x)
![example-1](https://github.com/KTBZay/Zayssystem/assets/97396492/40191c44-0880-4ec6-a920-5f427b517e27)
![exsample-2](https://github.com/KTBZay/Zayssystem/assets/97396492/ec860510-eecc-4765-bcb4-56bdb8309ea5)

# How to use (Express/Sqlite - 64x)
![exsample-4](https://github.com/KTBZay/Zayssystem/assets/97396492/91a6ec03-e423-414f-9cd3-0bb90fbc3625)
![example-5](https://github.com/KTBZay/Zayssystem/assets/97396492/1b317230-b940-41e3-81d6-baa5ccf9cff2)

See [Wiki](https://github.com/KTBZay/Zayssystem/wiki/Change-log) for more info on change log

This change log is the offical log for the npm version
